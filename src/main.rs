use clap::Parser;
use std::io::Write;
use std::time::Duration;
use std::{
    fs, io,
    io::{BufRead, BufReader},
};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long, value_parser)]
    file: String,
}

fn main() -> io::Result<()> {
    let args = Args::parse();

    let file = BufReader::new(fs::File::open(args.file)?)
        .lines()
        .filter_map(io::Result::ok)
        .collect::<Vec<String>>();

    let lines = file
        .iter()
        .map(|line| {
            // Split up into the timings and the content
            let pos1 = line.find('[').expect("Could not find '[' in a line") + 1;
            let pos2 = line.find(']').expect("Could not find ']' in a line");

            (&line[pos1..pos2], &line[pos2 + 1..])
        })
        .map(|line| {
            // Split the timings into two parts and cleanup
            (
                line.0.split('-').map(|x| x.trim()).collect::<Vec<_>>(),
                line.1.trim(),
            )
        })
        .map(|line| {
            // Parse the times into something useful
            let begin = line.0.get(0).expect("Failed to parse time");
            let end = line.0.get(1).expect("Failed to parse time");

            let b = begin.split(':').collect::<Vec<_>>();
            let e = end.split(':').collect::<Vec<_>>();

            let bs = b
                .get(0)
                .expect("Failed to parse time")
                .parse::<u64>()
                .expect("Failed to parse time")
                * 60
                + b.get(1)
                    .expect("Failed to parse time")
                    .parse::<u64>()
                    .expect("Failed to parse time");

            let es = e
                .get(0)
                .expect("Failed to parse time")
                .parse::<u64>()
                .expect("Failed to parse time")
                * 60
                + e.get(1)
                    .expect("Failed to parse time")
                    .parse::<u64>()
                    .expect("Failed to parse time");

            let d = Duration::new(es - bs, 0);

            (d, line.1)
        })
        .collect::<Vec<_>>();

    for line in lines {
        let len = line.1.len();
        
        if len == 0 {
            std::thread::sleep(line.0);
            println!();
        } else {
            let d = line.0 / line.1.len() as u32;

            for c in line.1.chars() {
                print!("{c}");
                io::stdout().flush().unwrap();
                std::thread::sleep(d);
            }
            println!();
        }
    }

    Ok(())
}
